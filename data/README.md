# Data 

This script requires the following files to be present in the `/data` directory. All these are included in `.gitignore` to avoid accidentally uploaded in a commit. 


| file name           | contents                                     |
|---------------------|----------------------------------------------|
| membership_list.csv | a csv of your member list                    |
| zoom_attendance.csv | zoom attendance report                       |
| exclude_emails.csv  | any emails you don't want to count from zoom |


these are references in `zoom_sacrament_attendance.py` as: 

```python
membersDF = pd.read_csv('data/membership_list.csv')
zoomDF = pd.read_csv('data/zoom_attendance.csv')
excludedDF = pd.read_csv('data/exclude_emails.csv')
```

## File Formats

The following formats (based on column order) are required for `zoom_sacrament_attendance.py` to work. At the moment as long as the required columns are included and named correctly (case-sensitive), it will work.

### `membership_list.csv`

I successfully created this csv by copying from the member list webpage in LCR and pasting into google sheets and downloading as csv

required columns:
- `Name`
- `Address`
- `email`

example: 
``` 
Name,Address,Phone,email
"Collingswoth, Jim","12870 SW 18th St
Lolligrad, California 97225",222-555-4220,
"Smith, Jordan","11255 SW Amie St
Lolligrad, California 97225",222-555-8833,jsmith8833@gmail.com
"Jones, Michael","13600 SW Crescent St
Apt 123
Lolligrad, California 97225",,
```

### `zoom_attendance.csv`

This is the unmodified csv downloaded from zoom's attendance report

required columns:
- `Email`

```
Attended,User Name (Original Name),First Name,Last Name,Email,Registration Time,Approval Status,Join Time,Leave Time,Time in Session (minutes),Country/Region Name
Yes,Griffith Park Ward,Griffith,Park Ward,griffithparkwardzoom@geemail.com,,,"Aug 15, 2021 10:23:55","Aug 15, 2021 11:30:02",67,The United States
```

### exclude_emails.csv

This is a list of emails you'd like to exclude from the count. For example the host email, if it is being hosted by a shared ward account. 

```
email
"griffithparkwardzoom@geemail.com"
```