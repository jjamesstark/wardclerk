import pandas as pd

# source data
membersDF = pd.read_csv("data/membership_list.csv")
zoomDF = pd.read_csv("data/zoom_attendance.csv")
excludedDF = pd.read_csv("data/exclude_emails.csv")

# transformed data

## households
householdsDF = membersDF.groupby("Address").count().reset_index()

## zoom attendance
zoom_renamed_DF = zoomDF.rename(columns={"Email": "email"})
zoom_deduped_DF = zoom_renamed_DF[["email"]].drop_duplicates().dropna(how="all")
zoom_cleaned_DF = pd.merge(zoom_deduped_DF, excludedDF, how="outer", indicator=True)
zoom_attended_DF = zoom_cleaned_DF.loc[zoom_cleaned_DF._merge == "left_only", ["email"]]

## member list
membersDF = membersDF[["Name", "Address", "email"]]

## joined data

attended_membersDF = zoom_attended_DF.join(membersDF.set_index("email"), on="email")
zoom_householdsDF = attended_membersDF[["Address"]].drop_duplicates().dropna(how="all")
sums_zoom_householdsDF = zoom_householdsDF.join(
    householdsDF.set_index("Address"), on="Address"
)

## counts

phone_attended_wo_households = zoomDF.isna().sum()["Email"]
email_attended_wo_households = attended_membersDF.isna().sum()["Name"]
attended_wo_households = phone_attended_wo_households + email_attended_wo_households
attended_w_households = sums_zoom_householdsDF.sum()["Name"]

print("\n=============================")
print("Total on Zoom: " + str(attended_wo_households + attended_w_households))
print("-- summed by household count:" + str(attended_w_households))
print("-- counted without household:" + str(attended_wo_households))
print("----------------------------\n")
