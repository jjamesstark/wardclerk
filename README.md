# A Programming Project for Ward Clerking

This project means to maintain and open source python scripts that make being a ward clerk easier. The scope of this project is limited to tasks which are not supported by LCR or MLS, but are nonetheless required. The first script in this project is designed to talley sacrament attendance with data from the Zoom webinar attendance report. 

## Zoom Sacrament Meeting Attendance 

`zoom_sacrament_attendance.py` automates the count of sacrament meeting attendance by Zoom webinar attendees based on household size if a matching household record can be found via email address. 
